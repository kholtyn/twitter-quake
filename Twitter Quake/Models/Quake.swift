//
//  Quake.swift
//  Twitter Quake
//
//  Created by AppStarter on 15.03.2017.
//  Copyright © 2017 Krzysztof Hołtyn. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

struct Quake: Mappable {
    var quakeID: String
    var magnitude: Double?
    var place: String?
    var time: Date?
    var numberOfFelt: Int?
    var alert: String?
    var tsunami: Bool?
    var significant: Int?
    var coordinates: CLLocation?
    
    init?(map: Map) {
        quakeID      = ""
        magnitude    = nil
        place        = nil
        time         = nil
        numberOfFelt = nil
        alert        = nil
        tsunami      = nil
        significant  = nil
        coordinates  = nil
    }
    
    mutating func mapping(map: Map) {
        quakeID      <- map["id"]
        magnitude    <- map["properties.mag"]
        place        <- map["properties.place"]
        time         <- (map["properties.time"], transformDateFromMilliseconds) // Long -> Date
        numberOfFelt <- map["properties.felt"]
        alert        <- map["properties.alert"] // ?? String -> Alert
        tsunami      <- (map["properties.tsunami"], transformBoolFromInt) // Int -> Bool
        significant  <- map["properties.sig"]
        coordinates  <- (map["geometry.coordinates"], transformLocationFromDoubles) // [Double] -> CLLocation
    }
    
    let transformDateFromMilliseconds = TransformOf<Date, Double>(fromJSON: { (millisecondsInterval: Double?) -> Date? in
        guard let millisecondsInterval = millisecondsInterval else { return nil }
        
        return Date(timeIntervalSince1970: TimeInterval(millisecondsInterval / 1000.0))
    }, toJSON: { (date: Date?) -> Double? in
        guard let date = date else { return nil }
        
        return Double(date.timeIntervalSince1970)
    })
    
    let transformBoolFromInt = TransformOf<Bool, Int>(fromJSON: { (value: Int?) -> Bool? in
        guard let value = value else { return nil }
        
        return value > 0
    }, toJSON: { (value: Bool?) -> Int? in
        guard let value = value else { return nil }
        
        return value ? 1 : 0
    })
    
    let transformLocationFromDoubles = TransformOf<CLLocation, [Double]>(fromJSON: { (values: [Double]?) -> CLLocation? in
        guard let values = values else { return nil }
        
        return CLLocation(latitude: values[0], longitude: values[1])
    }, toJSON: { (value: CLLocation?) -> [Double]? in
        guard let value = value else { return nil }
        
        return [value.coordinate.latitude, value.coordinate.longitude]
    })
}
