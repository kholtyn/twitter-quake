//
//  EarthquakeService.swift
//  Twitter Quake
//
//  Created by AppStarter on 15.03.2017.
//  Copyright © 2017 Krzysztof Hołtyn. All rights reserved.
//

import Foundation
import ObjectMapper

struct EarthquakeService {
    static func getEarthquakesAfter(numberOfDays days: Double, completion: @escaping (Result<[Quake], Error>) -> ()) {
        EarthquakeProvider.request(.quakesAfter(date: Date.dateSinceNow(days: days))) { result in
            switch result {
            case let .success(response):
                let statusCode = response.statusCode // Int - 200, 401, 500, etc
                print(statusCode)
                
                do {
                    let data = try response.mapJSON()
                    guard let JSON = data as? [String: Any] else { return }
                    guard let earthQuakesJSON = JSON["features"] as? [[String: Any]] else { return }
                    let earthQuakes = earthQuakesJSON.flatMap { Mapper<Quake>().map(JSON: $0) }
                    return completion(.Success(earthQuakes))
                } catch {
                    return completion(.Error(error))
                }
                
            case let .failure(error):
                return completion(.Error(error))
            }
        }
    }
}
