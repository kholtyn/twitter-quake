//
//  QuakeViewController.swift
//  Twitter Quake
//
//  Created by AppStarter on 05.03.2017.
//  Copyright © 2017 Krzysztof Hołtyn. All rights reserved.
//

import UIKit

final class QuakeViewController: UIViewController {
    fileprivate struct Constants {
        static let segueIdentifier = "Twitter"
    }
    
    fileprivate var earthQuakes: [Quake]? {
        didSet{
            tableView.reloadData()
        }
    }
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard earthQuakes == nil else { return }
        fetchEarthquakes()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.segueIdentifier, let twitterViewController = segue.destination as? TwitterViewController {
            twitterViewController.number = (sender as? Int)
        }
    }
    
    private func fetchEarthquakes() {
        EarthquakeService.getEarthquakesAfter(numberOfDays: 2) { result in
            switch result {
            case .Success(let earthQuakes):
                self.earthQuakes = earthQuakes
            case .Error(_): ()
            }
        }
    }
}

extension QuakeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Constants.segueIdentifier, sender: indexPath.row)
    }
}

extension QuakeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return earthQuakes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        guard let earthQuakes = earthQuakes else { return cell }
        cell.textLabel?.text = earthQuakes[indexPath.row].place
        return cell
    }
}
